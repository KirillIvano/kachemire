import { defineConfig } from 'astro/config';
import solidJs from '@astrojs/solid-js';
import sitemap from '@astrojs/sitemap';
import { dirname, resolve } from 'path';
import { fileURLToPath } from 'url';
import image from '@astrojs/image';
const __dirname = dirname(fileURLToPath(import.meta.url));

// define variables globally, so that not to import them in each file
const variablesImports = [
    '@import "@/vars/colors.scss";',
    '@import "@/vars/fonts.scss";',
    '@import "@/vars/screens.scss";',
    '@import "@/vars/spacing.scss";',
].join('\n');

// https://astro.build/config
export default defineConfig({
    server: {
        port: 8080,
    },
    integrations: [
        solidJs(),
        sitemap(),
        image({
            serviceEntryPoint: '@astrojs/image/sharp',
        }),
    ],
    outDir: 'public',
    publicDir: 'static',
    vite: {
        resolve: {
            alias: {
                '@/': `${resolve(__dirname, 'src')}/`,
            },
        },
        css: {
            preprocessorOptions: {
                scss: {
                    additionalData: variablesImports,
                },
            },
        },
    },
});
