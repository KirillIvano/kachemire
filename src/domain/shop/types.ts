export type ShopCityType = 'moscow' | 'sochi';

export type ShopInfo = {
    id: string;
    locationType: ShopCityType;
    location: string;
    coordinates: [lat: number, lon: number];
    phoneNumber?: string;
};
