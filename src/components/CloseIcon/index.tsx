import css from './styles.module.scss';
import closeIcon from './images/close.svg';
import cn from 'classnames';

export type Props = {
    onClick: () => void;
    class?: string;
};

export const CloseIcon = (props: Props) => (
    <button class={cn(css.icon, props.class)} onClick={props.onClick}>
        <img width={36} height={36} src={closeIcon} aria-hidden="true"></img>
    </button>
);
