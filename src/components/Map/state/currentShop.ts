import { createSignal } from 'solid-js';

export const [currentShop, setCurrentShop] = createSignal<null | string>(null);
