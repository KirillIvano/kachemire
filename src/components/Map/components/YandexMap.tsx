import { ShopInfo } from '@/domain/shop/types';
import { createEffect, createSignal } from 'solid-js';
import { currentShop, setCurrentShop } from '../state/currentShop';
import css from './styles.module.scss';

export type Props = {
    shops: ShopInfo[];
};

const loadMap = () => {
    const scriptElement = document.createElement('script');
    scriptElement.src =
        'https://api-maps.yandex.ru/2.1/?apikey=33e26001-aa75-4b32-98e5-c2b8696dbc3f&lang=ru_RU';
    scriptElement.type = 'text/javascript';

    document.documentElement.appendChild(scriptElement);

    return scriptElement;
};

const MOSCOW_COORDINATES = [55.751244, 37.618423];

export const YandexMap = (props: Props) => {
    const [map, setMap] = createSignal<ymaps.Map | null>(null);

    const init = () => {
        const newMap = new window.ymaps.Map(
            'map',
            {
                center: props.shops?.[0]?.coordinates ?? MOSCOW_COORDINATES,
                zoom: 12,
                controls: [],
            },
            {
                minZoom: 5,
            },
        );

        setMap(newMap);
    };

    createEffect(() => {
        const mapScript = loadMap();

        /** @todo добавить обработку */
        // mapScript.addEventListener('error', console.log);
        mapScript.addEventListener('load', () => {
            window.ymaps.ready(init);
        });
    });

    createEffect(() => {
        const currentMap = map();
        if (!currentMap) return;

        const currentShopItem = props.shops?.find(
            (s) => s.id === currentShop(),
        );

        if (!currentShopItem) return;

        currentMap.panTo(currentShopItem.coordinates, {
            flying: true,
            duration: 200,
        });
    });

    createEffect(() => {
        const currentMap = map();

        if (!currentMap || !props.shops) return;

        currentMap.geoObjects.removeAll();

        const firstShop = props.shops[0];
        if (!firstShop) return;

        currentMap.panTo(firstShop.coordinates, {
            flying: true,
            duration: 200,
        });

        for (const shop of props.shops) {
            const shopPlacemark = new window.ymaps.Placemark(
                shop.coordinates,
                {
                    balloonContent: `<div>${shop.location}</div>`,
                },
                {
                    preset: 'islands#circleIcon',
                    iconColor: '#7C503C',
                    balloonCloseButton: true,
                    hideIconOnBalloonOpen: true,
                    hasBalloon: true,
                },
            );

            shopPlacemark.events.add('click', () => {
                setCurrentShop(shop.id);
                currentMap.panTo(shop.coordinates, { flying: true });
            });

            currentMap.geoObjects.add(shopPlacemark);
        }
    });

    return <div id="map" class={css.map}></div>;
};
