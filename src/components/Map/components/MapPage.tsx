import { ShopCityType, ShopInfo } from '@/domain/shop/types';
import { Tabs } from '@/uikit/Tabs';
import { createMemo, createSignal, mapArray } from 'solid-js';
import { ShopList } from './ShopList';
import { YandexMap } from './YandexMap';
import css from './styles.module.scss';

export type Props = {
    shops: ShopInfo[];
};

const CITIES_OPTIONS = [
    { id: 'moscow', name: 'Москва' },
    { id: 'sochi', name: 'Сочи' },
];

export const MapPage = (props: Props) => {
    const [currentCity, setCurrentCity] = createSignal<ShopCityType>('moscow');
    const [citiesOptions] = createSignal(CITIES_OPTIONS);

    const filteredShops = createMemo(() =>
        props.shops.filter((x) => x.locationType === currentCity()),
    );

    const citiesTabs = mapArray(citiesOptions, (option) => {
        const selected = createMemo(() => option.id === currentCity());

        return { ...option, selected: selected };
    });

    return (
        <div>
            <Tabs
                class={css.tabs!}
                name="location_tabs"
                value={currentCity()}
                onChange={setCurrentCity}
                options={citiesTabs()}
            />

            <div class={css.mapRow}>
                <ShopList shops={filteredShops()} currentCity={currentCity()} />

                <YandexMap shops={filteredShops()} />
            </div>
        </div>
    );
};
