import { ShopCityType, ShopInfo } from '@/domain/shop/types';
import { createEffect, For } from 'solid-js';

import cn from 'classnames';
import css from './styles.module.scss';
import { currentShop, setCurrentShop } from '../state/currentShop';
import { LOCATION_TYPE_TO_NAME } from '@/data/shops';

export type Props = {
    shops: ShopInfo[];
    currentCity: ShopCityType;
};

const ShopListItem = (props: { shop: ShopInfo }) => (
    <li
        class={cn(css.listItem, {
            [css.active!]: currentShop() === props.shop.id,
        })}
        data-element={props.shop.id}
        onClick={() => setCurrentShop(props.shop.id)}
    >
        <p class={css.locationType}>
            {LOCATION_TYPE_TO_NAME[props.shop.locationType]}
        </p>
        <p class={css.location}>{props.shop.location}</p>
    </li>
);

export const ShopList = (props: Props) => {
    // eslint-disable-next-line prefer-const
    let containerRef: HTMLUListElement = undefined as any;

    createEffect(() => {
        const element = containerRef.querySelector(
            `[data-element="${currentShop()}"]`,
        );

        element &&
            element.scrollIntoView({
                behavior: 'smooth',
                block: 'nearest',
            });
    });

    return (
        <ul class={css.itemsList} ref={containerRef}>
            <For each={props.shops}>
                {(shop) => <ShopListItem shop={shop} />}
            </For>
        </ul>
    );
};
