import cn from 'classnames';

import { NavigationItem } from '@/shared/navigation';
import { Container } from '@/uikit/Container/Container';

import css from './styles.module.scss';
import { HeaderLogo } from '../HeaderLogo';

export type Props = {
    navigationItems: NavigationItem[];
    currentPath: string;
};

export const HeaderDesktop = (props: Props) => (
    <header class={css.header}>
        <Container class={css.headerContent}>
            <HeaderLogo />

            <ul class={css.navigation}>
                {props.navigationItems.map((route) => {
                    return (
                        <a
                            class={cn(css.navigationItem, {
                                [css.current!]:
                                    route.path === props.currentPath,
                            })}
                            href={route.path}
                        >
                            <li>{route.name}</li>
                        </a>
                    );
                })}
            </ul>

            <div class={css.contacts}>
                <p>
                    <a href="tel:+79104766569">8 910 476-65-69</a>
                </p>
                <p>
                    <a href="mailto:rostokmos@mail.ru">rostokmos@mail.ru</a>
                </p>
            </div>
        </Container>
    </header>
);
