import { Dialog } from '@/components/Dialog/Dialog';
import { NavigationItem } from '@/shared/navigation';
import { Container } from '@/uikit/Container/Container';
import { createSignal } from 'solid-js';
import { BurgerIcon } from '../BurgerIcon';
import css from './styles.module.scss';
import cn from 'classnames';
import { HeaderLogo } from '../HeaderLogo';
import { CloseIcon } from '@/components/CloseIcon';

export type Props = {
    navigationItems: NavigationItem[];
};

export const HeaderMobile = (props: Props) => {
    const [isOpened, setOpened] = createSignal(false);

    const toggleMenu = () => setOpened((s) => !s);
    const closeMenu = () => setOpened(false);

    return (
        <>
            <header class={css.heading}>
                <Container class={css.headingContent}>
                    <HeaderLogo />

                    <BurgerIcon onClick={toggleMenu} />
                </Container>
            </header>

            <Dialog
                contentClass={cn(css.dialogContent, {
                    [css.opened!]: isOpened(),
                })}
                opened={isOpened()}
                onClose={closeMenu}
                closeTimeoutMS={300}
            >
                <CloseIcon class={css.closeIcon!} onClick={closeMenu} />

                <ul class={css.navigation}>
                    {props.navigationItems.map((route) => {
                        return (
                            <a href={route.path}>
                                <li>{route.name}</li>
                            </a>
                        );
                    })}
                </ul>
            </Dialog>
        </>
    );
};
