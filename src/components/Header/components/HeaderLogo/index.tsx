import css from './styles.module.scss';

export const HeaderLogo = () => (
    <a href="/">
        <img
            class={css.logo}
            aria-hidden="true"
            alt="logo"
            src="/images/logo.png"
        >
            Je' taime
        </img>
    </a>
);
