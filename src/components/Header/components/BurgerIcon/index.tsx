import css from './styles.module.scss';

export type Props = {
    onClick: () => void;
};

export const BurgerIcon = (props: Props) => (
    <div
        role="button"
        aria-label="burger menu icon"
        classList={{ [css.container!]: true }}
        onClick={props.onClick}
        tabIndex={0}
        class={css.container}
    >
        <span></span>
        <span></span>
        <span></span>
    </div>
);
