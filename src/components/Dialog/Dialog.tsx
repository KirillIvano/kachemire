import { assertNotUndefined } from '@/utils/assert';
import { createEffect, createSignal, onCleanup, Show } from 'solid-js';
import css from './Dialog.module.scss';

import cn from 'classnames';

export type Props = {
    opened: boolean;
    children: JSX.Element;
    onClose: () => void;

    shouldCloseOnClick?: boolean;
    closeTimeoutMS?: number;
    dialogClass?: string | undefined;
    contentClass?: string | undefined;
};

export const Dialog = (props: Props) => {
    const [innerClosed, setInnerClosed] = createSignal(true);

    let dialogRef: HTMLDialogElement | undefined;
    let dialogContentRef: HTMLDivElement | undefined;
    let currentCloseTimer: number | undefined;

    const handleDialogClose = (e: {
        target: EventTarget | null;
        preventDefault: () => void;
    }) => {
        e.preventDefault();

        assertNotUndefined(dialogRef);
        assertNotUndefined(dialogContentRef);

        if (!e.target || dialogContentRef === e.target) return;
        if (dialogContentRef.contains(e.target as HTMLElement)) return;
        props.onClose();
    };

    // реагирует на изменение props.opened
    createEffect(() => {
        assertNotUndefined(dialogRef);

        if (props.opened) {
            currentCloseTimer && clearTimeout(currentCloseTimer);
            dialogRef.showModal();

            setInnerClosed(false);

            if (props.shouldCloseOnClick)
                document.addEventListener('click', handleDialogClose);
            dialogRef.addEventListener('cancel', handleDialogClose);

            document.getElementById('body')!.style.overflowY = 'hidden';
            document.body!.style.overflowY = 'hidden';

            onCleanup(() => {
                assertNotUndefined(dialogRef);

                currentCloseTimer = setTimeout(() => {
                    currentCloseTimer = undefined;
                    dialogRef?.close();

                    setInnerClosed(true);
                }, props.closeTimeoutMS) as unknown as number;

                if (props.shouldCloseOnClick)
                    document.removeEventListener('click', handleDialogClose);
                dialogRef.removeEventListener('cancel', handleDialogClose);

                document.getElementById('body')!.style.overflowY = 'auto';
                document.body.style.overflowY = 'auto';
            });
        }
    });

    return (
        <dialog
            class={cn(css.dialog, props.dialogClass)}
            classList={{ [css.closed!]: innerClosed() }}
            ref={dialogRef!}
        >
            <Show when={!innerClosed()}>
                <div class={props.contentClass} ref={dialogContentRef!}>
                    {props.children}
                </div>
            </Show>
        </dialog>
    );
};
