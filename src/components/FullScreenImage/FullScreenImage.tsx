import { createEffect, onCleanup } from 'solid-js';

import css from './FullScreenImage.module.scss';

export type Props = {
    src: string;
    children: JSX.Element;
};

const debounce = <TParams extends unknown[]>(
    fn: (args: TParams) => void,
    timeout: number,
) => {
    let current: null | number = null;

    return (...args: TParams) => {
        if (current) {
            clearTimeout(current);
        }

        current = setTimeout(fn, timeout, ...args) as unknown as number;
    };
};

export const FullscreenImage = ({ children }: Props) => {
    let containerRef: null | HTMLDivElement = null;
    const setContainerRef = (ref: HTMLDivElement) => (containerRef = ref);

    createEffect(() => {
        const handleWindowChange = debounce(() => {
            if (!containerRef) return null;
            const windowHeight = document.documentElement.clientHeight;
            const windowWidth = document.documentElement.clientHeight;

            /** @todo перейти на нормальные переменные */
            const isMobile = windowWidth <= 600;
            const headerHeight = isMobile ? 60 : 80;

            containerRef.style.height = `${windowHeight - headerHeight}px`;
        }, 300);

        window.addEventListener('resize', handleWindowChange);

        handleWindowChange();

        onCleanup(() =>
            window.removeEventListener('resize', handleWindowChange),
        );
    });

    return (
        <div class={css.container} ref={setContainerRef}>
            <img
                sizes="(max-width: 480px) 450px,
             (max-width: 800px) 800px,
             (max-width: 1080px) 1080px,
             (max-width: 1280px) 1280px,
             1600px"
                srcSet="/images/main_image/480.webp 480w,
             /images/main_image/800.webp 800w,
             /images/main_image/1080.webp 1080w,
             /images/main_image/1280.webp 1280w,
             /images/main_image/1600.webp 1600w"
                class={css.image}
                aria-hidden="true"
            />
            <div class={css.content}>{children}</div>
        </div>
    );
};
