import { brands } from '@/data/brands';
import { createEffect, createMemo, createSignal, onCleanup } from 'solid-js';
import { CloseIcon } from '../CloseIcon';
import { Dialog } from '../Dialog/Dialog';

import cn from 'classnames';
import css from './styles.module.scss';
import { ButtonLink } from '@/uikit/ButtonLink/ButtonLink';

export type BrandDialogProps = {
    opened: boolean;
    closeModal: () => void;
    currentBrand: string | null;
};

export const BrandDialog = (props: BrandDialogProps) => {
    const brandInfo = createMemo(() =>
        brands.find(({ id }) => id === props.currentBrand),
    );

    return (
        <Dialog
            opened={props.opened}
            onClose={props.closeModal}
            closeTimeoutMS={200}
            dialogClass={css.dialog}
            contentClass={cn(css.dialogContent, {
                [css.opened!]: props.opened,
            })}
        >
            <div>
                <CloseIcon class={css.closeIcon!} onClick={props.closeModal} />

                <p> {brandInfo()?.description}</p>

                <ButtonLink
                    class={css.brandLink}
                    theme="dark"
                    size="normal"
                    href={brandInfo()!.link}
                >
                    На сайт
                </ButtonLink>
            </div>
        </Dialog>
    );
};

export type Props = {
    children: JSX.Element;
};

export const DIALOG_ID_DATA_ATTRIBUTE = 'data-brand-id';
export const WithBrandDialog = (props: Props) => {
    let containerRef: HTMLDivElement | undefined;

    const [currentBrand, setCurrentBrand] = createSignal<string | null>(null);
    const [dialogOpened, setDialogOpened] = createSignal(false);

    const closeModal = () => setDialogOpened(false);
    const openModal = (brandId: string) => {
        setCurrentBrand(brandId);
        setDialogOpened(true);
    };

    createEffect(() => {
        const handleInnerClick = (e: MouseEvent) => {
            let currentElement: HTMLElement | null = e.target as HTMLElement;

            while (currentElement !== containerRef && currentElement) {
                if (currentElement.dataset['brandId']) {
                    openModal(currentElement.dataset['brandId']);
                }
                currentElement = (currentElement as HTMLElement).parentElement;
            }
        };

        containerRef!.addEventListener('click', handleInnerClick);

        onCleanup(() => {
            containerRef!.removeEventListener('click', handleInnerClick);
        });
    });

    return (
        <div ref={containerRef!}>
            <BrandDialog
                currentBrand={currentBrand()}
                opened={dialogOpened()}
                closeModal={closeModal}
            />

            {props.children}
        </div>
    );
};
