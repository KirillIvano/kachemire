import { Button } from '@/uikit/Button/Button';
import { Input, TextArea } from '@/uikit/Input';
import css from './styles.module.scss';
import cn from 'classnames';
import { createSignal, Show } from 'solid-js';
import { ErrorMessage } from '@/uikit/ErrorMessage';

export type Props = {
    class?: string;
};

const sendForm = (formResults: Record<string, string>) => {
    /** @todo типы лучше шарить, посмотреть в сторону nx */
    const body = {
        mailType: 'brand',
        formData: formResults,
    };

    return fetch('https://api.kashemir.shop/mail', {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify(body),
    });
};

type RequestStatus = 'init' | 'pending' | 'fail' | 'success';

export const PartnershipForm = (props: Props) => {
    const [status, setStatus] = createSignal<RequestStatus>('init');

    const handleFormSubmit = (e: SubmitEvent) => {
        e.preventDefault();

        const form = e.currentTarget as HTMLFormElement;
        const formData = new FormData(form);

        const formResults = Object.fromEntries(formData);

        sendForm(formResults as Record<string, string>)
            .then((res) => {
                if (res.ok) {
                    setStatus('success');
                } else {
                    setStatus('fail');
                }
            })
            .catch(() => {
                setStatus('fail');
            });
    };

    return (
        <form
            class={cn(css.formContainer, props.class)}
            onSubmit={handleFormSubmit}
        >
            <div class={css.formRow}>
                <Input
                    label="ФИО контактного лица"
                    wrapperClass={css.formInput}
                    required
                    placeholder="Как к вам обращаться?"
                    name="name"
                />
            </div>

            <div class={css.formRow}>
                <Input
                    label="Наименование бренда"
                    wrapperClass={css.formInput}
                    required
                    placeholder={'ООО "Пример"'}
                    name="brand"
                    type="text"
                />
            </div>

            <div class={css.formRow}>
                <Input
                    label="Ссылка на соцсети"
                    wrapperClass={css.formInput}
                    required
                    placeholder="http://example.com"
                    name="brand"
                    type="url"
                />
            </div>

            <div class={css.formRow}>
                <Input
                    label="Почтовый адрес"
                    wrapperClass={css.formInput}
                    placeholder="company@gmail.com"
                    name="email"
                    required
                    type="email"
                />
            </div>
            <div class={css.formRow}>
                <Input
                    label="Телефон"
                    wrapperClass={css.formInput}
                    placeholder="+7 999 999 99 99"
                    name="phoneNumber"
                    type="tel"
                />
            </div>

            <div class={css.formRow}>
                <TextArea
                    label="Комментарий"
                    wrapperClass={css.formInput}
                    placeholder="Хотите что-то добавить?"
                    name="comment"
                />
            </div>

            <div class={css.buttonRow}>
                <Button
                    disabled={status() === 'success'}
                    size="normal"
                    theme="dark"
                >
                    Подтвердить
                </Button>

                <Show when={status() === 'success'}>
                    <p class={css.successMessage}>Форма успешно отправлена</p>
                </Show>

                <Show when={status() === 'fail'}>
                    <ErrorMessage message="Не удалось отправить форму, попробуйте позже" />
                </Show>
            </div>
        </form>
    );
};
