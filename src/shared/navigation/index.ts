export type NavigationItem = {
    path: string;
    name: string;
};

export type RootNavigationItem = NavigationItem & {
    hasOwnPage?: boolean;
    children?: NavigationItem[];
};

export type CreateNavigationRoutesParams = {
    articlesRoutes: NavigationItem[];
};

/** Список основных путей */
export const createNavigationRoutes = ({
    articlesRoutes,
}: CreateNavigationRoutesParams): RootNavigationItem[] => [
    {
        path: '/brands',
        name: 'Бренды',
    },
    // {
    //     path: '/about',
    //     name: 'О нас',
    //     hasOwnPage: false,
    //     children: articlesRoutes,
    // },
    {
        path: '/shops',
        name: 'Магазины',
    },
    {
        path: '/join-us',
        name: 'Сотрудничество',
    },
];
