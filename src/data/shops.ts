import { ShopCityType, ShopInfo } from '@/domain/shop/types';

export const LOCATION_TYPE_TO_NAME: Record<ShopCityType, string> = {
    moscow: 'Москва',
    sochi: 'Сочи',
};

/**
 * Подумать, ваще такое стоит конечно выносить в админку / конфиг
 * Возможно, стоит сделать загрузку эксельки с валидацией
 */
export const shops: ShopInfo[] = [
    {
        id: 'mos_afimoll',
        locationType: 'moscow',
        location: 'Пресненская набережная д.2, ТРЦ "Афимолл"',
        phoneNumber: '+7 495 231-39-77',
        coordinates: [55.74916156898958, 37.53974249999992],
    },
    {
        id: 'mos_confiture',
        locationType: 'moscow',
        location: 'Долгопрудный, Лихачевский пр-т 64, ТЦ "Конфитюр"',
        phoneNumber: '+7 495 746-81-56',
        coordinates: [55.93127006883115, 37.49437749999998],
    },
    {
        id: 'soc_rosa_hutor_panorama',
        locationType: 'sochi',
        location: 'курорт "Роза Хутор", наб. Панорама д. 1',
        coordinates: [43.672961574550754, 40.29306049999991],
    },
    {
        id: 'soc_rosa_hutor_paseka',
        locationType: 'sochi',
        location: 'курорт "Роза Хутор", ул. Пасека д. 1',
        coordinates: [43.65936619064561, 40.321231318454764],
    },
    {
        id: 'soc_krasnaya_rixos',
        locationType: 'sochi',
        location:
            'курорт "Красная Поляна", ул. Созвездий д. 3, отель "Rixos Красная Поляна Сочи"',
        coordinates: [43.67065792995129, 40.26157598710243],
    },
    {
        id: 'soc_krasnaya_mariott',
        locationType: 'sochi',
        location:
            'курорт "Красная Поляна", наб. Времена Года д. 1, отель "Marriott Красная Поляна Сочи"',
        coordinates: [43.683758574578626, 40.256957499999906],
    },
    {
        id: 'soc_gasprom_grand',
        locationType: 'sochi',
        location:
            'курорт "Газпром", ул. Ачипсинская д. 16, "Гранд Отель Поляна", корпус В',
        coordinates: [43.6890120745671, 40.276181500000014],
    },
    {
        id: 'soc_gasprom_1389',
        locationType: 'sochi',
        location:
            'курорт "Газпром", ул. Ачипсинская д. 8/11, "Поляна 1389 Отель и СПА"',
        coordinates: [43.69575907458453, 40.31728849999999],
    },
    {
        id: 'soc_sirius_raddison',
        locationType: 'sochi',
        location:
            'ПГТ Сириус, ул. Голубая д. 1А, отель "Radisson Blu Resort & Congress Centre"',
        coordinates: [43.409978574521155, 39.93814549999999],
    },
];
