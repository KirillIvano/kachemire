export const mapObject = <T = unknown, R = unknown>(
    mapper: (x: T) => R,
    obj: Record<PropertyKey, T>,
): Record<PropertyKey, R> => {
    return Object.entries(obj).reduce((acc, [key, item]) => {
        acc[key] = mapper(item);
        return acc;
    }, {} as Record<PropertyKey, R>);
};
