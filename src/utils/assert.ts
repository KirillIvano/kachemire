export class AssertionError extends Error {}

export function assertNotNull<T>(obj: T | null, e?: string): asserts obj is T {
    if (obj === null) throw new AssertionError(e);
}

export function assertNotUndefined<T>(
    obj: T | undefined,
    e?: string,
): asserts obj is T {
    if (obj === undefined) throw new AssertionError(e);
}
