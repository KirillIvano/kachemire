import css from './Container.module.scss';

export type ContainerProps = JSX.HTMLAttributes<HTMLDivElement>;

export const Container = (props: ContainerProps) => {
    return <div {...props} class={`${props.class} ${css.container}`} />;
};
