import { splitProps } from 'solid-js';
import {
    getButtonStyling,
    SharedButtonProps,
} from '../shared/button/getButtonStyling';

export type Props = SharedButtonProps &
    JSX.ButtonHTMLAttributes<HTMLButtonElement>;

export const Button = (base: Props) => {
    const [local, others] = splitProps(base, ['class', 'classList']);
    const { classList } = getButtonStyling(base);

    return (
        <button
            {...others}
            class={local.class}
            classList={{
                ...classList,
                ...local.classList,
            }}
        />
    );
};
