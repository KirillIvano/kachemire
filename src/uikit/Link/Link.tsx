import cn from 'classnames';

export type LinkProps = {
    href: string;
    className?: string;
} & JSX.AnchorHTMLAttributes<HTMLAnchorElement>;

export const Link = ({ href, className, children }: LinkProps) => {
    <a class={cn(className)} href={href}>
        {children}
    </a>;
};
