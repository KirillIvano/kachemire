import { mergeProps, splitProps } from 'solid-js';

import css from './styles.module.scss';

export type SharedButtonProps = {
    theme: 'light' | 'dark';
    size?: 'small' | 'normal' | 'large';
};
const DEFAULT_PROPS = {
    size: 'normal',
} as const;

export const getButtonStyling = (base: SharedButtonProps) => {
    const props = mergeProps(DEFAULT_PROPS, base);
    const [local] = splitProps(props, ['theme', 'size']);

    return {
        classList: {
            [css.button!]: true,
            [css[`theme_${local.theme}`]!]: true,
            [css[`size_${local.size}`]!]: true,
        },
    };
};
