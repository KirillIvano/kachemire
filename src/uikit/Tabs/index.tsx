import css from './styles.module.scss';
import cn from 'classnames';

export type Props = {
    name: string;
    options: { id: string; name: string; selected: () => boolean }[];
    onChange: (val: string) => void;
    value: string;
    class?: string;
};

export const Tabs = (props: Props) => {
    return (
        <div class={cn(props.class, css.tabs)}>
            {props.options.map((item) => {
                return (
                    <label
                        class={css.tab}
                        classList={{ [css.active!]: item.selected() }}
                    >
                        {item.name}

                        <input
                            name={props.name}
                            class={css.input}
                            type="radio"
                            value={item.id}
                            onChange={() => props.onChange(item.id)}
                            checked={item.selected()}
                        />
                    </label>
                );
            })}
        </div>
    );
};
