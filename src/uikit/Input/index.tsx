import { splitProps } from 'solid-js';
import css from './styles.module.scss';
import cn from 'classnames';

export type InputProps = {
    label: string;
    wrapperClass?: string | undefined;
} & Omit<JSX.InputHTMLAttributes<HTMLInputElement>, 'classList'>;

export const Input = (props: InputProps) => {
    const [local, other] = splitProps(props, [
        'class',
        'wrapperClass',
        'label',
        'required',
    ]);

    return (
        <label class={cn(local.wrapperClass, css.wrapper)}>
            <p>
                {local.label}
                {local.required && '*'}
            </p>

            <input
                {...other}
                required={local.required ?? false}
                class={cn(local.class, css.input)}
            />
        </label>
    );
};

export type TextAreaProps = {
    label: string;
    wrapperClass?: string | undefined;
} & Omit<JSX.InputHTMLAttributes<HTMLTextAreaElement>, 'classList'>;

export const TextArea = (props: TextAreaProps) => {
    const [local, other] = splitProps(props, [
        'class',
        'wrapperClass',
        'label',
        'required',
    ]);

    return (
        <label class={cn(local.wrapperClass, css.wrapper)}>
            <p class={css.label}>
                {local.label}
                {local.required && '*'}
            </p>

            <textarea
                {...other}
                required={local.required ?? false}
                class={cn(local.class, css.input)}
            />
        </label>
    );
};
