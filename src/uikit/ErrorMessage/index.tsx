import cn from 'classnames';

import css from './styles.module.scss';

export type Props = {
    message: string;
    class?: string;
};

export const ErrorMessage = (props: Props) => {
    return <p class={cn(css.errorMessage, props.class)}>{props.message}</p>;
};
