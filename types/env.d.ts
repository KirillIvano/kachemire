/// <reference types="@astrojs/image/client" />

interface ImportMetaEnv {
    readonly PUBLIC_PATH_PREFIX: string;
}

interface ImportMeta {
    readonly env: ImportMetaEnv;
}

declare module '*.svg' {
    const content: string;
    export default content;
}
